# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import peewee as pw
import os

DATABASE = os.environ["DB_NAME"]

if os.environ["DB_TYPE"] == "postgres":

    password = os.environ["DB_PASSWORD"]

    user = os.environ["DB_USER"]

    host = os.environ["DB_HOST"]

    database = pw.PostgresqlDatabase(DATABASE,password=PASSWORD, user=USER, host=HOST)

else:

    database = pw.SqliteDatabase(DATABASE)


class BaseModel(pw.Model):

    class Meta:

        database = database
