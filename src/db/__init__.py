from .BaseModel import BaseModel, database

def init():

    database.connect()

    database.create_tables([])

    database.close()
